var adjectives = [
    "Spiritual", 
    "Demonic", 
    "Violent", 
    "Romantic", 
    "Utopian",
    "Dystopian",
    "Organic",
    "Post-Appocalyptic",
    "Solar Punk",
    "Underwater",
    "Zero Gravity",
    "Cyberutopian",
    "Historical",
    "Afrofuturist",
    "Whimsical",
    "Modern",
    "Campy",
    "Monstrous",
    "Spooky",
    "Creepy",
    "Classical",
    "Mystical",
    "Futuristic",
    "Angelic",
    "Rogue",
    "Haunted",
    "Glamourous",
    "Psychadelic",
    "Cutesie",
    "Divine",
    "Magical", 
    "Steam punk",
    "Evil",
    ];

var contexts = [
    "Monarchy",
    "Dictatorship",
    "Heist",
    "Intrigue",
    "Enigma",
    "Bureaucracy",
    "Treasure Hunt",
    "Revenge",
    "Dynasty",
    "Western",
    "Shoot-em-up",
    "Quest",
    "Roulette",
    "Puzzle",
    "Murder mystery",
    "Military",
    "Legacy",
    "Biosphere",
    "Paradise",
];    
var startAngle = 0;
var arc1 = Math.PI / (adjectives.length / 2);
var arc2 = Math.PI / (contexts.length / 2);
var spinTimeout1 = null;
var spinTimeout2 = null;

var spinArcStart = 10;
var spinTime1 = 0;
var spinTime2 = 0;
var spinTimeTotal1 = 0;
var spinTimeTotal2 = 0;

var ctx;

document.getElementById("spin1").addEventListener("click", spin);

document.getElementById("spin2").addEventListener("click", spin2);

window.addEventListener('keydown', input);

function input(event) {
  console.log("Keycode: " + event.keyCode);
  if (event.keyCode == "32"){
    spin();
    spin2();
  }
}


function byte2Hex(n) {
  var nybHexString = "0123456789ABCDEF";
  return String(nybHexString.substr((n >> 4) & 0x0F,1)) + nybHexString.substr(n & 0x0F,1);
}

function RGB2Color(r,g,b) {
	return '#' + byte2Hex(r) + byte2Hex(g) + byte2Hex(b);
}

function getColor(item, maxitem) {
  var phase = 0;
  var center = 128;
  var width = 127;
  var frequency = Math.PI*2/maxitem;
  
  red   = Math.sin(frequency*item+2+phase) * width + center;
  green = Math.sin(frequency*item+0+phase) * width + center;
  blue  = Math.sin(frequency*item+4+phase) * width + center;
  
  return RGB2Color(red,green,blue);
}

function drawRouletteWheel() {
  var canvas = document.getElementById("canvas");
  if (canvas.getContext) {
    var outsideRadius = 200;
    var textRadius = 160;
    var insideRadius = 125;

    ctx1 = canvas.getContext("2d");
    ctx1.clearRect(0,0,500,500);

    ctx1.strokeStyle = "black";
    ctx1.lineWidth = 2;

    ctx1.font = 'bold 8px Helvetica, Arial';

    for(var i = 0; i < adjectives.length; i++) {
      var angle = startAngle + i * arc1;
      //ctx.fillStyle = colors[i];
      ctx1.fillStyle = getColor(i, adjectives.length);

      ctx1.beginPath();
      ctx1.arc(250, 250, outsideRadius, angle, angle + arc1, false);
      ctx1.arc(250, 250, insideRadius, angle + arc1, angle, true);
      ctx1.stroke();
      ctx1.fill();

      ctx1.save();
      ctx1.shadowOffsetX = -1;
      ctx1.shadowOffsetY = -1;
      ctx1.shadowBlur    = 0;
      ctx1.shadowColor   = "rgb(220,220,220)";
      ctx1.fillStyle = "black";
      ctx1.translate(250 + Math.cos(angle + arc1 / 2) * textRadius, 
                    250 + Math.sin(angle + arc1 / 2) * textRadius);
      ctx1.rotate(angle + arc1 / 2 + Math.PI / 2);
      var text = adjectives[i];
      ctx1.fillText(text, -ctx1.measureText(text).width / 2, 0);
      ctx1.restore();
    } 

    //Arrow
    ctx1.fillStyle = "black";
    ctx1.beginPath();
    ctx1.moveTo(250 - 4, 250 - (outsideRadius + 5));
    ctx1.lineTo(250 + 4, 250 - (outsideRadius + 5));
    ctx1.lineTo(250 + 4, 250 - (outsideRadius - 5));
    ctx1.lineTo(250 + 9, 250 - (outsideRadius - 5));
    ctx1.lineTo(250 + 0, 250 - (outsideRadius - 13));
    ctx1.lineTo(250 - 9, 250 - (outsideRadius - 5));
    ctx1.lineTo(250 - 4, 250 - (outsideRadius - 5));
    ctx1.lineTo(250 - 4, 250 - (outsideRadius + 5));
    ctx1.fill();
  }
}

function drawRouletteWheel2() {
    var canvas = document.getElementById("canvas2");
    if (canvas.getContext) {
      var outsideRadius = 200;
      var textRadius = 160;
      var insideRadius = 125;
  
      ctx = canvas.getContext("2d");
      ctx.clearRect(0,0,500,500);
  
      ctx.strokeStyle = "black";
      ctx.lineWidth = 2;
  
      ctx.font = 'bold 12px Helvetica, Arial';
  
      for(var i = 0; i < contexts.length; i++) {
        var angle = startAngle + i * arc2;
        //ctx.fillStyle = colors[i];
        ctx.fillStyle = getColor(i, contexts.length);
  
        ctx.beginPath();
        ctx.arc(250, 250, outsideRadius, angle, angle + arc2, false);
        ctx.arc(250, 250, insideRadius, angle + arc2, angle, true);
        ctx.stroke();
        ctx.fill();
  
        ctx.save();
        ctx.shadowOffsetX = -1;
        ctx.shadowOffsetY = -1;
        ctx.shadowBlur    = 0;
        ctx.shadowColor   = "rgb(220,220,220)";
        ctx.fillStyle = "black";
        ctx.translate(250 + Math.cos(angle + arc2 / 2) * textRadius, 
                      250 + Math.sin(angle + arc2 / 2) * textRadius);
        ctx.rotate(angle + arc2 / 2 + Math.PI / 2);
        var text = contexts[i];
        ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
        ctx.restore();
      } 
  
      //Arrow
      ctx.fillStyle = "black";
      ctx.beginPath();
      ctx.moveTo(250 - 4, 250 - (outsideRadius + 5));
      ctx.lineTo(250 + 4, 250 - (outsideRadius + 5));
      ctx.lineTo(250 + 4, 250 - (outsideRadius - 5));
      ctx.lineTo(250 + 9, 250 - (outsideRadius - 5));
      ctx.lineTo(250 + 0, 250 - (outsideRadius - 13));
      ctx.lineTo(250 - 9, 250 - (outsideRadius - 5));
      ctx.lineTo(250 - 4, 250 - (outsideRadius - 5));
      ctx.lineTo(250 - 4, 250 - (outsideRadius + 5));
      ctx.fill();
    }
  }

function spin() {
  spinAngleStart = Math.random() * 10 + 10;
  spinTime1 = 0;
  spinTimeTotal1 = Math.random() * 3 + 4 * 1000;
  rotateWheel();
}

function spin2() {
  spinAngleStart = Math.random() * 10 + 10;
  spinTime2 = 0;
  spinTimeTotal2 = Math.random() * 3 + 4 * 1000;
  rotateWheel2();
}

function rotateWheel() {
  spinTime1 += 30;
  if(spinTime1 >= spinTimeTotal1) {
    stopRotateWheel();
    return;
  }
  var spinAngle = spinAngleStart - easeOut(spinTime1, 0, spinAngleStart, spinTimeTotal1);
  startAngle += (spinAngle * Math.PI / 180);
  drawRouletteWheel();
  spinTimeout1 = setTimeout('rotateWheel()', 30);
}

function rotateWheel2() {
    spinTime2 += 15;
    if(spinTime2 >= spinTimeTotal2) {
      stopRotateWheel2();
      return;
    }
    var spinAngle = spinAngleStart - easeOut(spinTime2, 0, spinAngleStart, spinTimeTotal2);
    startAngle += (spinAngle * Math.PI / 180);
    drawRouletteWheel2();
    spinTimeout2 = setTimeout('rotateWheel2()', 30);
  }

function stopRotateWheel() {
  clearTimeout(spinTimeout1);
  var degrees = startAngle * 180 / Math.PI + 90;
  var arcd = arc1 * 180 / Math.PI;
  var index = Math.floor((360 - degrees % 360) / arcd);
  console.log('here stopRotateWheel1');
  ctx1.save();
  ctx1.font = 'bold 30px Helvetica, Arial';
  var text1 = adjectives[index]
  ctx1.fillText(text1, 250 - ctx1.measureText(text1).width / 2, 250 + 10);
  ctx1.restore();
}

function stopRotateWheel2() {
    clearTimeout(spinTimeout2);
    var degrees = startAngle * 180 / Math.PI + 90;
    var arcd = arc2 * 180 / Math.PI;
    var index = Math.floor((360 - degrees % 360) / arcd);
    ctx.save();
    console.log('here stopRotateWheel2');
    ctx.font = 'bold 30px Helvetica, Arial';
    var text = contexts[index]
    ctx.fillText(text, 250 - ctx.measureText(text).width / 2, 250 + 10);
    ctx.restore();
  }

function easeOut(t, b, c, d) {
  var ts = (t/=d)*t;
  var tc = ts*t;
  return b+c*(tc + -3*ts + 3*t);
}

drawRouletteWheel();
drawRouletteWheel2();
